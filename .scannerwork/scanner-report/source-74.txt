import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../../../app/constanta.dart';
import '../../shared/widget/Item_product.dart';
import 'home_view_model.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
      builder: (context, model, child) => Scaffold(
          body: Padding(
        padding: const EdgeInsets.all(16),
        child: Container(
          margin: const EdgeInsets.only(top: 30),
          child: Column(
            children: [
              CarouselSlider(
                options: CarouselOptions(height: 150),
                items: imageHeader.map((i) {
                  return Builder(builder: (BuildContext context) {
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.symmetric(horizontal: 5),
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                              fit: BoxFit.fill, image: NetworkImage(i))),
                    );
                  });
                }).toList(),
              ),
              const SizedBox(
                height: 30,
              ),
              Expanded(
                child: InkWell(
                  onTap: model.buttonDetail,
                  child: ListView.builder(
                      itemCount: model.listProducts.length,
                      itemBuilder: (context, index) {
                        final product = model.listProducts[index];
                        return Card(
                          child: ItemProduct(
                            iconContainer:
                                const Icon(Icons.favorite_outline_outlined),
                            textPrice: "${product.price}",
                            textRating: "${product.rating}",
                            textTitle: product.title,
                            urlImage: product.thumbnail,
                            Activity: home,
                          ),
                        );
                      }),
                ),
              ),
              Visibility(
                visible:
                    model.listProducts.length == model.productResponse?.total
                        ? false
                        : true,
                child: SizedBox(
                  width: double.infinity,
                  child: TextButton(
                      style: TextButton.styleFrom(
                        backgroundColor: Colors.blue,
                        foregroundColor: Colors.white,
                      ),
                      onPressed: () => model.getProduct(
                          10, model.listProducts.last.id.toString()),
                      child: const Text("Load More")),
                ),
              )
            ],
          ),
        ),
      )),
      viewModelBuilder: () => HomeViewModel(),
      onModelReady: (model) => model.getProduct(10, "0"),
    );
  }
}
