import 'dart:ffi';

import 'package:dio/dio.dart';

import '../models/response/products_response.dart';
import 'logging.dart';

class DioClient {
  final Dio _dio = Dio(BaseOptions(
    baseUrl: 'https://dummyjson.com/',
    connectTimeout: 5000,
    receiveTimeout: 3000,
  ))
    ..interceptors.add(Logging());

  Future<ProductsResponse?> getProduct(int limit, String skip) async {
    ProductsResponse? productResponse;

    try {
      Response productDataResponse =
          await _dio.get('products?limit=$limit&skip=$skip');
      print("data Produk: ${productDataResponse.data}");
      productResponse = ProductsResponse.fromJson(productDataResponse.data);
    } on DioError catch (e) {
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
      } else {
        print('Error sending request!');
        print(e.message);
      }
    }

    return productResponse;
  }
}
