import 'package:dio/dio.dart';
import 'package:logger/logger.dart';

class Logging extends Interceptor {
  var logger = Logger();
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    logger.i("REQUEST[${options.method}] => URL: ${options.uri}");
    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    logger.i(
        "RESPONSE[${response.statusCode}] => URL: ${response.requestOptions.path}");
    return super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    logger.i(
        "ERROR[${err.response?.statusCode}] => URL: ${err.requestOptions.uri}");
    return super.onError(err, handler);
  }
}
