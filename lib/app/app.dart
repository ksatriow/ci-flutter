import 'package:stacked/stacked_annotations.dart';
import 'package:stacked_services/stacked_services.dart';

import '../ui/view/detail/detail.dart';
import '../ui/view/favorite/favorit.dart';
import '../ui/view/home/home.dart';
import '../ui/view/login/login.dart';

@StackedApp(routes: [
  MaterialRoute(page: LoginUi, initial: true),
  CupertinoRoute(page: HomeView),
  CupertinoRoute(page: Detail),
  CupertinoRoute(page: Favorit)
], dependencies: [
  LazySingleton(classType: NavigationService)
])
class AppSetup {}
