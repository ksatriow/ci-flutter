// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// StackedRouterGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:flutter/cupertino.dart' as _i6;
import 'package:flutter/material.dart';
import 'package:product/ui/view/detail/detail.dart' as _i4;
import 'package:product/ui/view/favorite/favorit.dart' as _i5;
import 'package:product/ui/view/home/home.dart' as _i3;
import 'package:product/ui/view/login/login.dart' as _i2;
import 'package:stacked/stacked.dart' as _i1;
import 'package:stacked_services/stacked_services.dart' as _i7;

class Routes {
  static const loginUi = '/';

  static const homeView = '/home-view';

  static const detail = '/Detail';

  static const favorit = '/Favorit';

  static const all = <String>{loginUi, homeView, detail, favorit};
}

class StackedRouter extends _i1.RouterBase {
  final _routes = <_i1.RouteDef>[
    _i1.RouteDef(Routes.loginUi, page: _i2.LoginUi),
    _i1.RouteDef(Routes.homeView, page: _i3.HomeView),
    _i1.RouteDef(Routes.detail, page: _i4.Detail),
    _i1.RouteDef(Routes.favorit, page: _i5.Favorit)
  ];

  final _pagesMap = <Type, _i1.StackedRouteFactory>{
    _i2.LoginUi: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const _i2.LoginUi(),
        settings: data,
      );
    },
    _i3.HomeView: (data) {
      return _i6.CupertinoPageRoute<dynamic>(
        builder: (context) => const _i3.HomeView(),
        settings: data,
      );
    },
    _i4.Detail: (data) {
      return _i6.CupertinoPageRoute<dynamic>(
        builder: (context) => const _i4.Detail(),
        settings: data,
      );
    },
    _i5.Favorit: (data) {
      return _i6.CupertinoPageRoute<dynamic>(
        builder: (context) => const _i5.Favorit(),
        settings: data,
      );
    }
  };

  @override
  List<_i1.RouteDef> get routes => _routes;
  @override
  Map<Type, _i1.StackedRouteFactory> get pagesMap => _pagesMap;
}

extension NavigatorStateExtension on _i7.NavigationService {
  Future<dynamic> navigateToLoginUi(
      [int? routerId,
      bool preventDuplicates = true,
      Map<String, String>? parameters,
      Widget Function(
              BuildContext, Animation<double>, Animation<double>, Widget)?
          transition]) async {
    return navigateTo<dynamic>(Routes.loginUi,
        id: routerId,
        preventDuplicates: preventDuplicates,
        parameters: parameters,
        transition: transition);
  }

  Future<dynamic> navigateToHomeView(
      [int? routerId,
      bool preventDuplicates = true,
      Map<String, String>? parameters,
      Widget Function(
              BuildContext, Animation<double>, Animation<double>, Widget)?
          transition]) async {
    return navigateTo<dynamic>(Routes.homeView,
        id: routerId,
        preventDuplicates: preventDuplicates,
        parameters: parameters,
        transition: transition);
  }

  Future<dynamic> navigateToDetail(
      [int? routerId,
      bool preventDuplicates = true,
      Map<String, String>? parameters,
      Widget Function(
              BuildContext, Animation<double>, Animation<double>, Widget)?
          transition]) async {
    return navigateTo<dynamic>(Routes.detail,
        id: routerId,
        preventDuplicates: preventDuplicates,
        parameters: parameters,
        transition: transition);
  }

  Future<dynamic> navigateToFavorit(
      [int? routerId,
      bool preventDuplicates = true,
      Map<String, String>? parameters,
      Widget Function(
              BuildContext, Animation<double>, Animation<double>, Widget)?
          transition]) async {
    return navigateTo<dynamic>(Routes.favorit,
        id: routerId,
        preventDuplicates: preventDuplicates,
        parameters: parameters,
        transition: transition);
  }
}
