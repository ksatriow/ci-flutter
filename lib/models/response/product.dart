// ignore: depend_on_referenced_packages
import 'package:json_annotation/json_annotation.dart';

part 'product.g.dart';

@JsonSerializable()
class Product {
  @JsonKey(name: "id")
  int id=0;

  @JsonKey(name: "title")
  String title="";

  @JsonKey(name: "description")
  String description="";

  @JsonKey(name: "price")
  int price=0;

  @JsonKey(name: "discountPercentage")
  double discountPercentage=0.0;

  @JsonKey(name: "rating")
  double rating=0.0;

  @JsonKey(name: "stock")
  int stock=0;

  @JsonKey(name: "brand")
  String brand="";

  @JsonKey(name: "category")
  String category="";

  @JsonKey(name: "thumbnail")
  String thumbnail="";

  @JsonKey(name: "images")
  List<String> images=[];

  Product();

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);
  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
