import 'package:json_annotation/json_annotation.dart';
import 'package:product/models/response/product.dart';

part 'productsResponse.g.dart';

@JsonSerializable()
class ProductsResponse {
  @JsonKey(name: "products")
  List<Product> products = [];

  @JsonKey(name: "total")
  int total = 0;

  @JsonKey(name: "skip")
  String skip = "";

  @JsonKey(name: "limit")
  int limit = 0;

  ProductsResponse();

  factory ProductsResponse.fromJson(Map<String, dynamic> json) =>
      _$ProductsResponseFromJson(json);
  Map<String, dynamic> toJson() => _$ProductsResponseToJson(this);
}
