// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import '../../../app/constanta.dart';

class ItemProduct extends StatefulWidget {
  final Widget iconContainer;
  final String Activity;
  final String? urlImage;
  final String? textTitle;
  final String? textPrice;
  final String? textRating;

  const ItemProduct(
      {super.key,
      required this.iconContainer,
      this.urlImage,
      this.textTitle,
      this.textPrice,
      this.textRating,
      required this.Activity});

  @override
  State<ItemProduct> createState() => _ItemProductState();
}

class _ItemProductState extends State<ItemProduct> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        padding: const EdgeInsets.all(8),
        width: double.infinity,
        margin: const EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  blurRadius: 4,
                  spreadRadius: 2,
                  offset: const Offset(0, 3))
            ]),
        child: Row(
          children: <Widget>[
            Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage(widget.urlImage ??
                          'https://i.picsum.photos/id/104/3840/2160.jpg?hmac=Rv0qxBiYb65Htow4mdeDlyT5kLM23Z2cDlN53YYldZU'))),
            ),
            const SizedBox(
              width: 20,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("${widget.textTitle}",
                    overflow: TextOverflow.ellipsis, maxLines: 2),
                Text("Rp ${widget.textPrice}"),
                Text("Rating ${widget.textRating}")
              ],
            ),
            const Spacer(),
            Column(
              children: [
                IconButton(
                    onPressed: () {
                      if (widget.Activity == home) {
                        var snackBar = const SnackBar(
                            content: Text("Disimpan di Favorit"));
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      } else {
                        var snackBar =
                            const SnackBar(content: Text("Berhasil Dihapus"));
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      }
                    },
                    icon: widget.iconContainer),
              ],
            )
          ],
        ),
      ),
    );
    throw UnimplementedError();
  }
}
