import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../app/app.router.dart';
import '/../../app/app.locator.dart';

class DetailViewModel extends BaseViewModel {
  final navigationService = locator<NavigationService>();

  void buttonFavorit() {
    navigationService.navigateTo(Routes.favorit);
  }
}
