// ignore: file_names
import 'package:flutter/material.dart';

import '../../shared/widget/item_product.dart';
import '../../../app/constanta.dart';

class Favorit extends StatefulWidget {
  const Favorit({Key? key}) : super(key: key);

  @override
  State<Favorit> createState() => _FavoritState();
}

class _FavoritState extends State<Favorit> {
  String _dropdownValue = "Harga";

  void dropdownCallback(String? selectedValue) {
    if (selectedValue is String) {
      setState(() {
        _dropdownValue = selectedValue;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => Navigator.of(context).pop(),
          icon: const Icon(Icons.arrow_back, color: Colors.white),
        ),
        title: Text("Title"),
        actions: [
          IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.favorite,
                color: Colors.white,
              ))
        ],
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(left: 30, right: 30, top: 20),
                padding: EdgeInsets.only(left: 10, right: 10),
                decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black,
                    ),
                    borderRadius: const BorderRadius.all(Radius.circular(10))),
                child: DropdownButton(
                  isExpanded: true,
                  items: dropDownOptions
                      .map<DropdownMenuItem<String>>((String mascot) {
                    return DropdownMenuItem<String>(
                        value: mascot, child: Text(mascot));
                  }).toList(),
                  value: _dropdownValue,
                  onChanged: dropdownCallback,
                  style: const TextStyle(
                    color: Colors.blue,
                  ),
                ),
              ),
              // ignore: prefer_const_constructors
              SizedBox(
                height: 50,
              ),
              Expanded(
                  child: ListView.builder(
                      itemCount: imageHeader.length,
                      itemBuilder: (context, index) {
                        return Card(
                          child: ItemProduct(
                            iconContainer: const Icon(Icons.delete),
                            textPrice: "120000",
                            textRating: "5/5",
                            textTitle: "Tas",
                            urlImage: imageHeader.elementAt(index),
                            Activity: favorit,
                          ),
                        );
                      }))
            ],
          ),
        ),
      ),
    );
  }
}
