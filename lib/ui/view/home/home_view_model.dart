import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../APIs/dio_client.dart';
import '../../../app/app.locator.dart';
import '../../../app/app.router.dart';
import '../../../models/response/product.dart';
import '../../../models/response/products_response.dart';

class HomeViewModel extends BaseViewModel {
  final navigationService = locator<NavigationService>();

  final DioClient client = DioClient();

  ProductsResponse? productResponse;

  List<Product> listProducts = [];

  void buttonDetail() {
    navigationService.navigateTo(Routes.detail);
  }

  void getHttp(int limit, String skip) async {
    try {
      var response = await Dio()
          .get('https://dummyjson.com/products?limit=$limit&skip=$skip');
      productResponse = ProductsResponse.fromJson(response.data);
      listProducts.addAll(productResponse!.products);
      print("data: ${listProducts.length} limit= ${productResponse?.limit}");
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  void getProduct(int limit, String skip) async {
    productResponse = await client.getProduct(limit, skip);
    listProducts.addAll(productResponse!.products);
    notifyListeners();
  }
}
