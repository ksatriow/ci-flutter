import 'package:flutter/cupertino.dart';
import 'package:product/app/app.locator.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../app/app.router.dart';

class LoginViewModel extends BaseViewModel {
  final navigationService = locator<NavigationService>();
  bool passwordVisible = false;

  void buttonNext() {
    navigationService.navigateTo(Routes.homeView);
  }
}
